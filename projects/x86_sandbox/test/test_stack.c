#include "stack.h"

#include "unity.h"

void setUp(void) { stack_reset(); }

void tearDown(void) {}

void test_stack_reset(void) { TEST_ASSERT_EQUAL(0, stack_get_count()); }

void test_stack_get_count(void) {
  int pop_value;
  TEST_ASSERT_EQUAL(0, stack_get_count());
  stack_push(1);
  TEST_ASSERT_EQUAL(1, stack_get_count());
  stack_push(2);
  TEST_ASSERT_EQUAL(2, stack_get_count());
  stack_push(3);
  TEST_ASSERT_EQUAL(3, stack_get_count());
  stack_pop(&pop_value);
  TEST_ASSERT_EQUAL(2, stack_get_count());
  stack_pop(&pop_value);
  TEST_ASSERT_EQUAL(1, stack_get_count());
  stack_pop(&pop_value);
  TEST_ASSERT_EQUAL(0, stack_get_count());
}

void test_stack_push(void) {
  TEST_ASSERT_TRUE(stack_push(4));
  for (int i = 1; i < 10; i++) {
    stack_push(i);
  }
  TEST_ASSERT_FALSE(stack_push(5));
}

void test_stack_pop(void) {
  int pop_value;
  TEST_ASSERT_FALSE(stack_pop(&pop_value));

  stack_push(6);
  TEST_ASSERT_TRUE(stack_pop(&pop_value));
  TEST_ASSERT_EQUAL(6, pop_value);
}
