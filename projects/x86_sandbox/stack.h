#pragma once

#include <stdbool.h>
#include <stddef.h>

/**
 * Create a stack to hold integer variables
 * You can use a static int array[10] to hold up to 10 integers
 */

/**
 * Resets the stack such that it has zero values
 */
void stack_reset(void);

/**
 * @return the number of variables stack can hold
 */
size_t stack_get_count(void);

/**
 * @return true if value was successfully pushed, or false if stack is full
 */
bool stack_push(int value_to_push);

/**
 * @return true if a value was popped
 */
bool stack_pop(int *value_to_pop);
