#include "stack.h"

static int array[10];
size_t top = 0;

void stack_reset(void) { top = 0; }
size_t stack_get_count(void) { return top; }

bool stack_push(int value_to_push) {
  bool status = false;
  const size_t array_size = sizeof(array) / sizeof(array[0]);
  if (top < (array_size - 1)) {
    status = true;
    array[top] = value_to_push;
    top++;
  }
  return status;
}

bool stack_pop(int *value_to_pop) {
  // int pop_val = 0;
  bool status = false;
  if (top > 0) {
    *value_to_pop = array[top - 1];
    status = true;
    --top;
  }
  return status;
}
